#MAC0110 - MiniEP7
#Eike Souza - NUSP: 4618653

function bernoulli(n)
    n *= 2
    A = Vector{Rational{BigInt}}(undef, n + 1)
    for m = 0: n
        A[m + 1] = 1 // (m + 1)
        for j = m : -1 : 1
            A[j] = j * (A[j] - A[j + 1])
        end
    end
    return abs(A[1])
end

function sin(x)
    soma = 0
    somap = x
    termo = x
    n = 1
    fat = 1
    while n <= 10
        soma = soma + somap
        termo = termo * (-1) * x * x
        fat = big(fat * 2 * n * (2 * n + 1))
        somap = termo/fat
        n = n + 1
    end
    return soma
end

function cos(x)
    soma = 1
    somap = 0
    termo = 1
    n = 1
    fat = 1
    while n <= 10
        soma = soma + somap
        termo = termo * (-1) * x * x
        fat = big(fat * 2 * n * (2 * n - 1))
        somap = termo/fat
        n = n + 1
    end
    return soma
end

function tan(x)
    soma = 0
    somap = x
    termo = x
    n = 1
    while n <= 10
        if n >= 2
            soma = soma + somap
            termo = termo*x*x
            somap = ((2^(2*n))*(2^(2*n) - 1)*termo*bernoulli(n))/big(factorial(2*n))
        end
        n = n + 1
    end
    return soma
end

function check_sin(value, x)
    erro = 0.001
    if abs(value - sin(x)) <= erro
        return true
    else
        return false
    end
end

function check_cos(value, x)
    erro = 0.001
    if abs(value - cos(x)) <= erro
        return true
    else
        return false
    end
end

function check_tan(value, x)
    erro = 0.001
    if abs(value - tan(x)) <= erro
        return true
    else
        return false
    end
end

function taylor_sin(x)
    soma = 0
    somap = x
    termo = x
    n = 1
    fat = 1
    while n <= 10
        soma = soma + somap
        termo = termo * (-1) * x * x
        fat = big(fat * 2 * n * (2 * n + 1))
        somap = termo/fat
        n = n + 1
    end
    return soma
end

function taylor_cos(x)
    soma = 1
    somap = 0
    termo = 1
    n = 1
    fat = 1
    while n <= 10
        soma = soma + somap
        termo = termo * (-1) * x * x
        fat = big(fat * 2 * n * (2 * n - 1))
        somap = termo/fat
        n = n + 1
    end
    return soma
end

function taylor_tan(x)
    soma = 0
    somap = x
    termo = x
    n = 1
    while n <= 10
        if n >= 2
            soma = soma + somap
            termo = termo*x*x
            somap = ((2^(2*n))*(2^(2*n) - 1)*termo*bernoulli(n))/big(factorial(2*n))
        end
        n = n + 1
    end
    return soma
end

function test()
    if sin(0) != 0.0
        println("Nao funcionou para sen(0)")
    end
    if sin(pi/2) < 0.98
        println("Não funcionou para sen(pi/2)")
    end
    if sin(3*(pi/2)) > 0
        println("Não funcionou para sen(3*(pi/2)")
    end

    if cos(0) != 1.0
        println("Não funcionou para cos(0)")
    end
    if cos(pi) > 0
        println("Não funcionou para cos(pi)")
    end
    if cos(pi/2) != 0.0
        println("Não funcionou para cos(pi/2)")
    end

    if tan(pi/4) > 1
        println("Não funcionou para tg(pi/4)")
    end
    if tan(0) != 0
        println("Não funcionou para tg(0)")
    end

    if check_sin(1, pi/2) != true
        println("Algo errado para seno de pi/2")
    end

    if check_sin(0,0) != true
        println("Algo errado para seno de 0")
    end

    if check_cos(0,1) != true
        println("Algo errado para cosseno de zero")
    end

    if check_tan(0,0) != true
        println("Algo errado para tangente de zero")
    end

    println("Fim dos testes")
end

test()
